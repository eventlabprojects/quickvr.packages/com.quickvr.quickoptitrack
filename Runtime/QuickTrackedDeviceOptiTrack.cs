using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuickVR.OptiTrack
{
    public class QuickTrackedDeviceOptiTrack : QuickTrackedDevice
    {

        #region PUBLIC ATTRIBUTES

        public int _motiveBoneID
        {
            get; protected set;
        }

        protected OptitrackSkeletonState _skeletonState
        {
            get
            {
                return ((QuickTrackingManagerBodyOptiTrack)_trackingManager)._skeletonState;
            }
        }

        #endregion

        #region CREATION AND DESTRUCTION

        public virtual void Init(int motiveBoneID)
        {
            _motiveBoneID = motiveBoneID;
        }

        #endregion

        protected override bool IsTracking()
        {
            return base.IsTracking() && _skeletonState != null;
        }

        public override Vector3 GetTrackedPosition()
        {
            _skeletonState.BonePoses.TryGetValue(_motiveBoneID, out OptitrackPose bonePose);
            return bonePose.Position;
        }

        public override Quaternion GetTrackedRotation()
        {
            _skeletonState.BonePoses.TryGetValue(_motiveBoneID, out OptitrackPose bonePose);
            Quaternion rotOffset;
            if (_motiveBoneID == 9)
            {
                rotOffset = Quaternion.Euler(0, 90, 0);
            }
            else if (_motiveBoneID == 13)
            {
                rotOffset = Quaternion.Euler(0, 270, 0);
            }
            else
            {
                rotOffset = Quaternion.Euler(0, 180, 0);
            }

            return bonePose.Orientation * rotOffset;
        }

    }
}
