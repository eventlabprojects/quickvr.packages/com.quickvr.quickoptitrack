﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace QuickVR.OptiTrack
{

    public class QuickTrackingManagerBodyOptiTrack : QuickTrackingManagerBody
    {

        #region PUBLIC ATTRIBUTES

        public OptitrackSkeletonState _skeletonState
        {
            get; protected set;
        }

        #endregion

        #region PROTECTED ATTRIBUTES

        protected OptitrackStreamingClient _streamingClient = null;

        /// <summary>The streamed source skeleton definition.</summary>
        protected OptitrackSkeletonDefinition m_skeletonDef;

        protected static Dictionary<int, QuickHumanBodyBones> _toQuickHumanBodyBones
        {
            get
            {
                if (m_ToQuickHumanBodyBones == null)
                {
                    m_ToQuickHumanBodyBones = new Dictionary<int, QuickHumanBodyBones>();

                    m_ToQuickHumanBodyBones[0] = QuickHumanBodyBones.Invalid;

                    m_ToQuickHumanBodyBones[1] = QuickHumanBodyBones.Hips;
                    m_ToQuickHumanBodyBones[2] = QuickHumanBodyBones.Spine;
                    m_ToQuickHumanBodyBones[3] = QuickHumanBodyBones.Chest;
                    m_ToQuickHumanBodyBones[4] = QuickHumanBodyBones.Neck;
                    m_ToQuickHumanBodyBones[5] = QuickHumanBodyBones.Head;

                    m_ToQuickHumanBodyBones[6] = QuickHumanBodyBones.LeftShoulder;
                    m_ToQuickHumanBodyBones[7] = QuickHumanBodyBones.LeftUpperArm;
                    m_ToQuickHumanBodyBones[8] = QuickHumanBodyBones.LeftLowerArm;
                    m_ToQuickHumanBodyBones[9] = QuickHumanBodyBones.LeftHand;

                    m_ToQuickHumanBodyBones[10] = QuickHumanBodyBones.RightShoulder;
                    m_ToQuickHumanBodyBones[11] = QuickHumanBodyBones.RightUpperArm;
                    m_ToQuickHumanBodyBones[12] = QuickHumanBodyBones.RightLowerArm;
                    m_ToQuickHumanBodyBones[13] = QuickHumanBodyBones.RightHand;

                    m_ToQuickHumanBodyBones[14] = QuickHumanBodyBones.LeftUpperLeg;
                    m_ToQuickHumanBodyBones[15] = QuickHumanBodyBones.LeftLowerLeg;
                    m_ToQuickHumanBodyBones[16] = QuickHumanBodyBones.LeftFoot;
                    m_ToQuickHumanBodyBones[17] = QuickHumanBodyBones.LeftToes;

                    m_ToQuickHumanBodyBones[18] = QuickHumanBodyBones.RightUpperLeg;
                    m_ToQuickHumanBodyBones[19] = QuickHumanBodyBones.RightLowerLeg;
                    m_ToQuickHumanBodyBones[20] = QuickHumanBodyBones.RightFoot;
                    m_ToQuickHumanBodyBones[21] = QuickHumanBodyBones.RightToes;

                    m_ToQuickHumanBodyBones[22] = QuickHumanBodyBones.LeftThumbProximal;
                    m_ToQuickHumanBodyBones[23] = QuickHumanBodyBones.LeftThumbIntermediate;
                    m_ToQuickHumanBodyBones[24] = QuickHumanBodyBones.LeftThumbDistal;
                    m_ToQuickHumanBodyBones[25] = QuickHumanBodyBones.RightThumbProximal;
                    m_ToQuickHumanBodyBones[26] = QuickHumanBodyBones.RightThumbIntermediate;
                    m_ToQuickHumanBodyBones[27] = QuickHumanBodyBones.RightThumbDistal;

                    m_ToQuickHumanBodyBones[28] = QuickHumanBodyBones.LeftIndexProximal;
                    m_ToQuickHumanBodyBones[29] = QuickHumanBodyBones.LeftIndexIntermediate;
                    m_ToQuickHumanBodyBones[30] = QuickHumanBodyBones.LeftIndexDistal;
                    m_ToQuickHumanBodyBones[31] = QuickHumanBodyBones.RightIndexProximal;
                    m_ToQuickHumanBodyBones[32] = QuickHumanBodyBones.RightIndexIntermediate;
                    m_ToQuickHumanBodyBones[33] = QuickHumanBodyBones.RightIndexDistal;

                    m_ToQuickHumanBodyBones[34] = QuickHumanBodyBones.LeftMiddleProximal;
                    m_ToQuickHumanBodyBones[35] = QuickHumanBodyBones.LeftMiddleIntermediate;
                    m_ToQuickHumanBodyBones[36] = QuickHumanBodyBones.LeftMiddleDistal;
                    m_ToQuickHumanBodyBones[37] = QuickHumanBodyBones.RightMiddleProximal;
                    m_ToQuickHumanBodyBones[38] = QuickHumanBodyBones.RightMiddleIntermediate;
                    m_ToQuickHumanBodyBones[39] = QuickHumanBodyBones.RightMiddleDistal;

                    m_ToQuickHumanBodyBones[40] = QuickHumanBodyBones.LeftRingProximal;
                    m_ToQuickHumanBodyBones[41] = QuickHumanBodyBones.LeftRingIntermediate;
                    m_ToQuickHumanBodyBones[42] = QuickHumanBodyBones.LeftRingDistal;
                    m_ToQuickHumanBodyBones[43] = QuickHumanBodyBones.RightRingProximal;
                    m_ToQuickHumanBodyBones[44] = QuickHumanBodyBones.RightRingIntermediate;
                    m_ToQuickHumanBodyBones[45] = QuickHumanBodyBones.RightRingDistal;

                    m_ToQuickHumanBodyBones[46] = QuickHumanBodyBones.LeftLittleProximal;
                    m_ToQuickHumanBodyBones[47] = QuickHumanBodyBones.LeftLittleIntermediate;
                    m_ToQuickHumanBodyBones[48] = QuickHumanBodyBones.LeftLittleDistal;
                    m_ToQuickHumanBodyBones[49] = QuickHumanBodyBones.RightLittleProximal;
                    m_ToQuickHumanBodyBones[50] = QuickHumanBodyBones.RightLittleIntermediate;
                    m_ToQuickHumanBodyBones[51] = QuickHumanBodyBones.RightLittleDistal;
                }

                return m_ToQuickHumanBodyBones;
            }
        }
        protected static Dictionary<int, QuickHumanBodyBones> m_ToQuickHumanBodyBones = null;

        #endregion 

        #region CREATION AND DESTRUCTION

        protected override void Awake()
        {
            _boneOrigin = QuickHumanBodyBones.Hips;

            _streamingClient = QuickSingletonManager.GetInstance<OptitrackStreamingClient>();
            _streamingClient.RegisterSkeleton(this, name);

            // Retrieve the OptiTrack skeleton definition.
            m_skeletonDef = _streamingClient.GetSkeletonDefinitionByName(name);

            base.Awake();
        }

        public override void CreateTrackedDevices()
        {
            if (m_skeletonDef != null)
            {
                for (int boneDefIdx = 0; boneDefIdx < m_skeletonDef.Bones.Count; ++boneDefIdx)
                {
                    OptitrackSkeletonDefinition.BoneDefinition boneDef = m_skeletonDef.Bones[boneDefIdx];

                    QuickHumanBodyBones qBoneID = _toQuickHumanBodyBones[boneDef.Id];
                    if (qBoneID != QuickHumanBodyBones.LastBone)
                    {
                        if (qBoneID != QuickHumanBodyBones.LeftShoulder && qBoneID != QuickHumanBodyBones.RightShoulder)
                        {
                            CreateTrackedDevice<QuickTrackedDeviceOptiTrack>(qBoneID).Init(boneDef.Id);
                        }
                    }
                    else
                    {
                        Debug.LogError(GetType().FullName + ": Could not find QuickHumanBodyBone for Motive bone ID \"" + boneDef.Id + "\"", this);
                    }
                }

                for (int boneDefIdx = 0; boneDefIdx < m_skeletonDef.Bones.Count; ++boneDefIdx)
                {
                    OptitrackSkeletonDefinition.BoneDefinition boneDef = m_skeletonDef.Bones[boneDefIdx];
                    _skeleton.SetBoneParenting(_toQuickHumanBodyBones[boneDef.Id], _toQuickHumanBodyBones[boneDef.ParentId]);
                }
            }
            else
            {
                Debug.LogError(GetType().FullName + ": Could not find skeleton definition with the name \"" + name + "\"", this);
            }
        }

        #endregion

        #region GET AND SET

        public override bool IsEnabled()
        {
            return base.IsEnabled() && _streamingClient != null && m_skeletonDef != null;
        }

        #endregion

        protected override void UpdateTrackingImp()
        {
            _skeletonState = _streamingClient.GetLatestSkeletonState(m_skeletonDef.Id);

            base.UpdateTrackingImp();
        }

    }

}


