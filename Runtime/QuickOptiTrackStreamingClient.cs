using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace QuickVR.OptiTrack
{
    public class QuickOptiTrackStreamingClient : OptitrackStreamingClient
    {

        #region PROTECTED ATTRIBUTES

        protected string _defaultIP
        {
            get
            {
                if (m_DefaultIP.Length == 0)
                {
                    m_DefaultIP = GetDefaultNetworkIPAddress();
                }

                return m_DefaultIP;
            }
        }
        protected string m_DefaultIP = "";

        #endregion

        #region CONSTANTS

        protected const string LOCAL_IP_ADDRESS = "127.0.0.1";

        #endregion

        #region CREATION AND DESTRUCTION

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void Init()
        {
            QuickSingletonManager.GetInstance<QuickOptiTrackStreamingClient>();
        }

        protected virtual void Awake()
        {
            //For some reason, if ServerAddress or LocalAddress is the default 127.0.0.1, 
            //the connection fails. So we replace it by the first available IPv4 address if any. 
            if (ServerAddress == LOCAL_IP_ADDRESS)
            {
                ServerAddress = _defaultIP;
            }            
            if (LocalAddress == LOCAL_IP_ADDRESS)
            {
                LocalAddress = _defaultIP;
            }
        }

        static string GetDefaultNetworkIPAddress()
        {
            NetworkInterface defaultInterface = GetDefaultInterface();
            string result = String.Empty;

            if (defaultInterface != null)
            {
                // Retrieve and display its IP addresses
                string[] ipAddresses = GetInterfaceIPAddresses(defaultInterface);
                //foreach (string ip in ipAddresses)
                //{
                //    Debug.Log("ip = " + ip);
                //}

                if (ipAddresses.Length > 0)
                {
                    result = ipAddresses[0];
                }
                else
                {
                    Debug.LogError("DEfault network interface does not have a valid IP Address");
                }
            }
            else
            {
                Debug.Log("Default network interface not found.");
            }

            return result;
        }

        static NetworkInterface GetDefaultInterface()
        {
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                IPInterfaceProperties ipProperties = networkInterface.GetIPProperties();

                // Check if the interface has a gateway defined
                if (ipProperties.GatewayAddresses.Any())
                {
                    return networkInterface; // Return the first interface with a gateway
                }
            }

            return null; // Return null if no interface with a gateway is found
        }

        static string[] GetInterfaceIPAddresses(NetworkInterface networkInterface)
        {
            IPInterfaceProperties ipProperties = networkInterface.GetIPProperties();

            var ipv4Addresses = ipProperties.UnicastAddresses
                .Where(address => address.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                .Select(address => address.Address.ToString());

            //foreach (string ipv4Address in ipv4Addresses)
            //{
            //    Debug.Log($"IPv4 Address: {ipv4Address}");
            //}

            return ipv4Addresses.ToArray<string>();
        }

        #endregion

        [ButtonMethod]
        public virtual void Test()
        {
            QuickSingletonManager.GetInstance<QuickTrackingManagerBodyOptiTrack>();
        }

    }

}


